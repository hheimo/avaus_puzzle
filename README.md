# avaus_puzzle

Solution to Avaus' technical interview in 2019.

## Problem

Check the numbers.txt, add signs: + - \* (plus, minus, times) between numbers (there are 1000 of them) to make a whole equation hold true. Show your computations.

## Solution

In file Arithmetic Puzzle.ipynb
